import pygame
import random
from Queue import Queue, deque
from pygame.color import Color
import numpy as np
from util import PriorityQueue
import timeit
import time
import sys

d = 30
WIDTH = 20
HEIGHT = 20
stone_num=10

LEFT, RIGHT, UP, DOWN = -1, 1, -WIDTH, WIDTH
DIRECTION = {-1: 'LEFT', 1: 'RIGHT', -WIDTH: 'UP', WIDTH: 'DOWN'}
MOVES = (LEFT, RIGHT, UP, DOWN)
INF = WIDTH*HEIGHT+1
DFS_FAIL_LIMIT = 10

snake = deque()
stone = deque()
fruit = None
dfs_fail_count = 0
weight_horizontal=1
weight_vertical=1
ri = random.randint(0,1)


def weight_matrix():
    Dimension = HEIGHT*WIDTH
    a = np.zeros((Dimension,Dimension))
    for pos in range(Dimension):
        if pos % WIDTH > 0:
            a[pos,pos - 1] = weight_horizontal
            a[pos - 1, pos] = weight_horizontal
        if pos % WIDTH < WIDTH-1:
            a[pos, pos + 1] = weight_horizontal
            a[pos + 1, pos] = weight_horizontal
        if pos >= WIDTH:
            a[pos, pos - WIDTH] = weight_vertical
            a[pos - WIDTH, pos] = weight_vertical
        if pos < (HEIGHT-1)*WIDTH:
            a[pos, pos + WIDTH] = weight_vertical
            a[pos + WIDTH, pos] = weight_vertical
    return a


def is_move_possible(pos, move):
    if move == LEFT:
        return pos % WIDTH > 0
    elif move == RIGHT:
        return pos % WIDTH < WIDTH-1
    elif move == UP:
        return pos >= WIDTH
    elif move == DOWN:
        return pos < (HEIGHT-1)*WIDTH


def get_best_moves(pos, dist, func):
    possible_moves = [mv for mv in MOVES if pos+mv not in list(snake)[:-1]
                      and mv for mv in MOVES if pos+mv not in list(stone)
                      and is_move_possible(pos, mv)]
    if not possible_moves:
        return []
    ds = [dist[pos+mv] for mv in possible_moves if dist[pos+mv] != INF]
    if not ds:
        return []
    return [mv for mv in possible_moves if dist[pos+mv] == func(ds)]

def dij_reach(target, snake):
    q = PriorityQueue()
    q.push(target,0)
    found = False
    dist = [INF]*(HEIGHT*WIDTH)
    dist[target] = 0

    weight = weight_matrix()
    while not q.isEmpty():
        pos = q.pop()
        for mv in MOVES:
            if is_move_possible(pos, mv):
                new_pos = pos + mv

                if new_pos == snake[0]:
                    found = True
                if new_pos not in snake and new_pos not in stone:
                    if dist[new_pos] == INF:
                        dist[new_pos] = dist[pos] + weight[pos,new_pos]
                        q.push(new_pos,dist[new_pos])
                    elif dist[new_pos] > dist[pos] + weight[pos,new_pos]:
                        dist[new_pos] = dist[pos] + weight[pos,new_pos]
                        q.push(new_pos,dist[new_pos])
    return found, dist


def brave(snake, dist):
    global dfs_fail_count
    if dfs_fail_count > DFS_FAIL_LIMIT:  # blance between speed and distance
        return False, None
    if snake[0] == fruit:
        ok, _ = dij_reach(snake[-1], snake)
        ok = ok and any(snake[0]+mv not in snake for mv in MOVES) and any(snake[0]+mv not in stone for mv in MOVES)
        if not ok:
            dfs_fail_count += 1
        return ok, None

    best_moves = get_best_moves(snake[0], dist, min)
    if not best_moves:
        return False, None
    random.shuffle(best_moves)

    for mv in best_moves:
        snake.appendleft(snake[0]+mv)
        if snake[0] != fruit:
            t = snake.pop()
        ok, _ = brave(snake, dist)
        if snake[0] != fruit:
            snake.append(t)
        snake.popleft()
        if ok:
            return True, mv
    return False, None

def next_step():
    global dfs_fail_count
    dfs_fail_count = 0
    ok, dist = dij_reach(fruit, snake)
    if ok:
        ok, mv = brave(snake, dist)
    if not ok:  # try to chase the tail and select the longest path
        _, dist = dij_reach(snake[-1], snake)
        best_moves = get_best_moves(snake[0], dist, min)
        if not best_moves:
            print "LOSE"
            time.sleep(60)
            sys.exit(0)
        mv = random.choice(best_moves)
    # move the snake
    snake.appendleft(snake[0]+mv)
    if snake[0] != fruit:
        snake.pop()
    return DIRECTION[mv]

def draw(window, my_image, straw_image,apple_image,snake_head,snake_body,stone_image):
    image_rect=my_image.get_rect()
    window.blit(my_image,image_rect)

    
    if ri==0:
        window.blit(straw_image,((fruit % WIDTH)*d, (fruit / WIDTH)*d))
    elif ri==1:
        window.blit(apple_image,((fruit % WIDTH)*d, (fruit / WIDTH)*d))
    
    n = len(snake)
    for i, pos in enumerate(snake):
        if i==0:
            window.blit(snake_head,((pos % WIDTH)*d, (pos / WIDTH)*d))
        else:   
            window.blit(snake_body,((pos % WIDTH)*d, (pos / WIDTH)*d))

    for i, pos in enumerate(stone):
        window.blit(stone_image,((pos % WIDTH)*d, (pos / WIDTH)*d))
    
    ws="weight_horizontal:"+str(weight_horizontal)+"  weight_vertical:"+str(weight_vertical)
    myfont = pygame.font.SysFont("monospace",15)

    label = myfont.render(ws,1,(255,0,0))
    window.blit(label,(1,1))
    pygame.display.flip()

def main():
    global fruit
    global ri
    global weight_horizontal
    global weight_vertical

    pygame.init()
    
    window = pygame.display.set_mode((d*WIDTH, d*HEIGHT))
    my_image=pygame.image.load("grass.png")
    my_image=pygame.transform.scale(my_image,(WIDTH*d,HEIGHT*d))
    straw_image=pygame.image.load("straw.png")
    straw_image=pygame.transform.scale(straw_image,(2*d,2*d))
    apple_image=pygame.image.load("apple.png")
    apple_image=pygame.transform.scale(apple_image,(2*d,2*d))
    snake_head=pygame.image.load("snake_head.png")
    snake_head=pygame.transform.scale(snake_head,(int(1.2*d),int(1.2*d)))
    snake_body=pygame.image.load("snake_body.png")
    snake_body=pygame.transform.scale(snake_body,(d,d))
    stone_image=pygame.image.load("stone.png")
    stone_image=pygame.transform.scale(stone_image,(d,d))

    pygame.display.set_caption('Snake AI')
    fruit = random.randint(0, WIDTH*HEIGHT-1)
    snake.append(random.randint(0, WIDTH*HEIGHT-1))
    clock = pygame.time.Clock()

    stone_start=random.randint(0,WIDTH*HEIGHT-1-stone_num)
    for i in range(stone_num):
        stone.append(stone_start+i)

    start=time.time()
    fruit_num=1
   
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    weight_horizontal+=10
                if event.key == pygame.K_s:
                    weight_vertical+=10

        if fruit is not None:
            next_step()
        if snake[0] == fruit:
            possible_fruit = set(xrange(WIDTH*HEIGHT)) - set(snake) - set(stone)
            if not possible_fruit:
                fruit = None
            else:
                fruit = random.choice(list(possible_fruit))
                fruit_num += 1
            ri = random.randint(0, 1)

        if fruit:
            draw(window, my_image, straw_image,apple_image,snake_head,snake_body,stone_image)
            pygame.display.update()
        
        
        clock.tick(50)

    end=time.time()
    print "time: ", end-start
    print "fruit: ", fruit_num
main()